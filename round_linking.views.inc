<?php

/**
 * @file
 * Provides module's Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function round_linking_views_data_alter(&$data) {
  $entity_types = \Drupal::entityTypeManager()->getDefinitions();

  foreach ($entity_types as $entity_type) {
    $table = $entity_type->getDataTable();

    if ($table && isset($data[$table])) {
      $id_field = $entity_type->getKey('id');

      $data[$table]["{$entity_type->id()}_round_linking"] = [
        'title' => t('Round Linking: @label ID', ['@label' => ucfirst(strtolower($entity_type->getLabel()))]),
        'argument' => [
          'field' => $id_field,
          'id' => 'round_linking',
        ],
      ];
    }
  }
}
