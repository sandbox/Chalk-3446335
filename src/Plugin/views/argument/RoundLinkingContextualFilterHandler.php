<?php

namespace Drupal\round_linking\Plugin\views\argument;

use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Custom Round Linking Contextual Filter.
 *
 * @ViewsArgument("round_linking")
 */
class RoundLinkingContextualFilterHandler extends NumericArgument {

  const ROUND_LINKING_UNION = 'ROUND_LINKING_UNION';
  const ROUND_LINKING_UNION_DONE = 'ROUND_LINKING_UNION_DONE';

  /**
   * {@inheritDoc}
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();

    $placeholder = $this->placeholder();
    $null_check = empty($this->options['not']) ? '' : " OR $this->tableAlias.$this->realField IS NULL";
    $operator = '>';
    $field = "$this->tableAlias.$this->realField";

    $this->query->addTag(self::ROUND_LINKING_UNION);
    $this->query->addWhereExpression(0, "$field $operator $placeholder" . $null_check, [$placeholder => $this->argument]);
  }

}
