## Round Linking

Round linking module accepts node ID as an argument and displays subsequent content nodes. 

## Table of contents

- Requirements
- Known issues
- Installation
- Configuration

## Requirements

This module requires the following modules:

- [Views](https://www.drupal.org/project/views)

## Known issues

Sort criteria in the Views UI tends to break the logic of this module, therefore it is advised not to use any sorting criteria.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will allow you to use custom contextual filter "Round Linking: Content ID" in the Views UI.
