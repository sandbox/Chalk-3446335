<?php

namespace Drupal\Tests\round_linking\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test checking the existence of a custom plugin.
 *
 * @group round_linking
 */
class RoundLinkingPluginExistTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['round_linking', 'views'];

  /**
   * Tests the existence of a custom plugin.
   */
  public function testCustomPluginExists() {
    $plugin_manager = $this->container->get('plugin.manager.views.argument');

    $plugin_exists = $plugin_manager->hasDefinition('round_linking');

    $this->assertTrue($plugin_exists, "Custom plugin doesn't exist.");
  }

}
