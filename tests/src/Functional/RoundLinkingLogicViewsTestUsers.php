<?php

namespace Drupal\Tests\round_linking\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\block\Entity\Block;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;

/**
 * Round Linking logic tests.
 *
 * @group round_linking
 */
class RoundLinkingLogicViewsTestUsers extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['user', 'node', 'round_linking', 'views', 'round_linking_test', 'block'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The created users.
   *
   * @var \Drupal\user\Entity\User[]
   */
  protected $users;

  /**
   * Tests existence of a view.
   */
  public function testViewExistence() {
    $view_name = 'round_linking_test_users';
    $entity_type_manager = \Drupal::entityTypeManager();
    $view_storage = $entity_type_manager->getStorage('view');
    $view = $view_storage->load($view_name);
    $this->assertNotNull($view);
  }

  /**
   * Tests block functionality for users.
   */
  public function testViewBlock() {
    $anonymous_role = Role::load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('access user profiles');
    $anonymous_role->save();

    $this->users = [];
    for ($i = 1; $i <= 9; $i++) {
      $user = User::create(
            [
              'name' => 'Placeholder' . $i,
              'mail' => $this->randomMachineName() . '@example.com',
              'status' => 1,
            ]
        );
      $user->save();
      $user->setUsername('Test User ' . $user->id());
      $user->save();
      $this->users[$user->id()] = $user;
      $this->assertTrue($user instanceof User);
      $this->drupalGet('/user/' . $user->id());
      $this->assertSession()->statusCodeEquals(200);
    }

    $block = $this->drupalPlaceBlock('views_block:round_linking_test_users-block_1');
    $block = Block::load($block->id());
    $this->assertNotNull($block);
    $block->setRegion('content');
    $block->save();

    $admin_user = User::load(1);
    $admin_user->setUsername('Test User 1');
    $admin_user->save();

    $this->drupalGet('/user/6');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Test User 7');
    $this->assertSession()->pageTextContains('Test User 8');
    $this->assertSession()->pageTextContains('Test User 9');
    $this->assertSession()->pageTextContains('Test User 10');

    $this->drupalGet('/user/8');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Test User 9');
    $this->assertSession()->pageTextContains('Test User 10');
    // Admin.
    $this->assertSession()->pageTextContains('Test User 1');
    $this->assertSession()->pageTextContains('Test User 2');
  }

}
