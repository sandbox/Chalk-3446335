<?php

namespace Drupal\Tests\round_linking\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\block\Entity\Block;
use Drupal\node\NodeInterface;

/**
 * Round Linking logic tests.
 *
 * @group round_linking
 */
class RoundLinkingLogicViewsTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'round_linking', 'views', 'round_linking_test', 'block'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The created nodes.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodes;

  /**
   * Tests existence of a view.
   */
  public function testViewExistence() {
    $view_name = 'round_linking_test';
    $entity_type_manager = \Drupal::entityTypeManager();
    $view_storage = $entity_type_manager->getStorage('view');
    $view = $view_storage->load($view_name);
    $this->assertNotNull($view);
  }

  /**
   * Tests block.
   */
  public function testViewBlock() {
    $this->createContentType(['type' => 'article']);
    $this->nodes = [];
    for ($i = 1; $i <= 10; $i++) {
      $node = $this->drupalCreateNode(
            [
              'type' => 'article',
              'title' => 'Test Article ' . $i,
              'body' => [
                'value' => 'The body of a test article ' . $i,
                'format' => 'basic_html',
              ],
            ]
        );
      $node->save();
      $this->nodes[$node->id()] = $node;
      $node->setPublished();
      $this->assertTrue($node instanceof NodeInterface);
      $this->drupalGet('/node/' . $i);
      $this->assertSession()->statusCodeEquals(200);
    }
    $block = $this->drupalPlaceBlock('views_block:round_linking_test-block_1');
    $block = Block::load($block->id());
    $this->assertNotNull($block);
    $block->setRegion('content');
    $block->save();

    $this->drupalGet('/node/6');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Test Article 7');
    $this->assertSession()->pageTextContains('Test Article 8');
    $this->assertSession()->pageTextContains('Test Article 9');
    $this->assertSession()->pageTextContains('Test Article 10');

    $this->drupalGet('/node/8');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Test Article 9');
    $this->assertSession()->pageTextContains('Test Article 10');
    $this->assertSession()->pageTextContains('Test Article 1');
    $this->assertSession()->pageTextContains('Test Article 2');
  }

}
